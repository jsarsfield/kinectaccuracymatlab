function testF( vec, col )
    %TESTF Summary of this function goes here
    %   Detailed explanation goes here
    if ~exist('col','var')
        quiver3(0,0,0,vec(1),vec(2),vec(3),'AutoScale','off');
    else
        quiver3(0,0,0,vec(1),vec(2),vec(3),'AutoScale','off','color',col);
    end
    hold on;
end

