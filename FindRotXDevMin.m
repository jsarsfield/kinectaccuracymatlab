function [ UNew ] = FindRotXDevMin(  kinectPositions, qualisysPositions, kinectGT, qualisysGT, U )
 % Rotate around X to find rotation that minimises deviation of
 % wristleft joint at arms to side and arms tpose
    global Utemp;
    lowestMean = [];
    UNew = [];
    qPosReshape = reshape(qualisysPositions',3,[])';
    qGTReshape = reshape(qualisysGT',3,[])';
    kPosReshape = reshape(kinectPositions',3,[])';
    kGTReshape = reshape(kinectGT',3,[])';
    
    %Norm
    qPosReshape(:,1) = qPosReshape(:,1)-qGTReshape(1,1);
    qPosReshape(:,2) = qPosReshape(:,2)-qGTReshape(1,2);
    qPosReshape(:,3) = qPosReshape(:,3)-qGTReshape(1,3);
    
    qGTReshape(:,1) = qGTReshape(:,1)-qGTReshape(1,1);
    qGTReshape(:,2) = qGTReshape(:,2)-qGTReshape(1,2);
    qGTReshape(:,3) = qGTReshape(:,3)-qGTReshape(1,3);
    
    kPosReshape(:,1) = kPosReshape(:,1)-kGTReshape(1,1);
    kPosReshape(:,2) = kPosReshape(:,2)-kGTReshape(1,2);
    kPosReshape(:,3) = kPosReshape(:,3)-kGTReshape(1,3);
    
    kGTReshape(:,1) = kGTReshape(:,1)-kGTReshape(1,1);
    kGTReshape(:,2) = kGTReshape(:,2)-kGTReshape(1,2);
    kGTReshape(:,3) = kGTReshape(:,3)-kGTReshape(1,3);
    
    theta = pi/2;  
    R = [1,0,0;0 cos(theta) -sin(theta);0 sin(theta) cos(theta)];
    rot = R*[kPosReshape(:,1)';kPosReshape(:,2)';kPosReshape(:,3)'];
    kPosReshape(:,1) = rot(1,:);
    kPosReshape(:,2) = rot(2,:);
    kPosReshape(:,3) = rot(3,:);
    rot = R*[kGTReshape(:,1)';kGTReshape(:,2)';kGTReshape(:,3)'];
    kGTReshape(:,1) = rot(1,:);
    kGTReshape(:,2) = rot(2,:);
    kGTReshape(:,3) = rot(3,:);

    theta = pi;
    R = [cos(theta),-sin(theta),0; sin(theta),cos(theta),0; 0,0,1];
    rot = R*[kPosReshape(:,1)';kPosReshape(:,2)';kPosReshape(:,3)'];
    kPosReshape(:,1) = rot(1,:);
    kPosReshape(:,2) = rot(2,:);
    kPosReshape(:,3) = rot(3,:);
    rot = R*[kGTReshape(:,1)';kGTReshape(:,2)';kGTReshape(:,3)'];
    kGTReshape(:,1) = rot(1,:);
    kGTReshape(:,2) = rot(2,:);
    kGTReshape(:,3) = rot(3,:);
    
    kPosReshape = reshape(kPosReshape',39,[])';
    kGTReshape = reshape(kGTReshape',39,[])';
    
    %Wrist joints Z (X,Z,Y) - [19,20,21]
    j = [20,29];
    angle = 40;
    for i = -angle:0.2:angle
        Utemp = U*rotx(i);
        % Rotate qPos & qGT about x axis
        qPosTemp = arrayfun(@RotateDataset,qPosReshape(:,1),qPosReshape(:,2),qPosReshape(:,3),'UniformOutput',0); 
        qGTTemp = arrayfun(@RotateDataset,qGTReshape(:,1),qGTReshape(:,2),qGTReshape(:,3),'UniformOutput',0);
        qPosTemp = cell2mat(qPosTemp);
        qGTTemp = cell2mat(qGTTemp);
        qPosTemp = reshape(qPosTemp',39,[])';
        qGTTemp = reshape(qGTTemp',39,[])';
        % Get z diff
        zDiff = sum(abs((kGTReshape(j)-qGTTemp(j))-(kPosReshape(j)-qPosTemp(j))));
        if (i == -angle)
            lowestMean = zDiff;
            UNew = Utemp;
            continue;
        end
        if (zDiff < lowestMean)
            lowestMean = zDiff;
            UNew = Utemp;
        end
    end
end

function point = RotateDataset(x,y,z)
    global Utemp;
    point = (Utemp*[x;y;z])';
end
