function [] = TrimActions( KSkel, MSkel, filename )
    f = figure
    MSkelOffset = 1;
    KSkelOffset = 1;
    bClicked = false;

    ms = uicontrol(f,'Style','slider',...
                'Min',1,'Max',length(MSkel),'Value',1,...
                'Position',[30 30 250 30],...
                'Callback',@MSkelSlider,'SliderStep',[1/length(MSkel),1/length(MSkel)]);
    ks = uicontrol(f,'Style','slider',...
        'Min',1,'Max',length(KSkel),'Value',1,...
        'Position',[30 0 250 30],...
        'Callback',@KSkelSlider,'SliderStep',[1/length(KSkel),1/length(KSkel)]);
   
    uicontrol('Style','text',...
        'Position',[400 20 120 20],...
        'String',0,'Tag','MSkelTime');
    uicontrol('Style','text',...
        'Position',[400 0 120 20],...
        'String',0,'Tag','KSkelTime');
    
    uicontrol('Style', 'pushbutton', 'String', 'KSkel Start',...
        'Position', [0 70 80 20],...
        'Callback', @KSkelStart);  
    uicontrol('Style', 'pushbutton', 'String', 'MSkel Start',...
        'Position', [0 90 80 20],...
        'Callback', @MSkelStart); 
    
    uicontrol('Style', 'pushbutton', 'String', 'Save Skel',...
        'Position', [0 200 80 20],...
        'Callback', @Save); 
    uicontrol('Style', 'pushbutton', 'String', 'Find Start',...
        'Position', [0 220 80 20],...
        'Callback', @FindStart); 
    
    function FindStart(hObject, EventData)
        mFrame = round(get(ms,'value'));
        kFrame = round(get(ks,'value'));
        % find index where wristright y axis decreases.
        for n = mFrame:length(MSkel)
            if (MSkel(n,29) > MSkel(n+1,29))
                mFrame = n;
                break;
            end
        end
        
        for n = kFrame:length(KSkel)
            if (KSkel(n,29) > KSkel(n+1,29))
                kFrame = n;
                break;
            end
        end
        [v,mFrame] = max(MSkel(:,30));
        [v,kFrame] = max(KSkel(:,30));
        set(ms,'value',mFrame);
        set(ks,'value',kFrame);
        Draw();
    end
    function Save(hObject, EventData)
        save(strcat('data/',strcat(filename,'mat')),'KSkel', 'MSkel');
    end
    function MSkelStart(hObject, EventData)
        MSkelOffset = round(get(ms,'value'));
        MSkel = MSkel(MSkelOffset:end,:);
        timeOffset = MSkel(1,1);
        MSkel(:,1) = MSkel(:,1)-timeOffset;
    end
    function KSkelStart(hObject, EventData)
        KSkelOffset = round(get(ks,'value'));
        KSkel = KSkel(KSkelOffset:end,:);
        timeOffset = KSkel(1,1);
        KSkel(:,1) = KSkel(:,1)-timeOffset;
    end

    function MSkelSlider(hObject, eventdata, handles)
        set(hObject, 'Value', round(get(ms,'value')));
        Draw
    end
    function KSkelSlider(hObject, eventdata, handles)
        set(hObject, 'Value', round(get(ks,'value')));
        Draw
    end
    function Draw()
        mSliderVal = get(ms,'value');
        kSliderVal = get(ks,'value');
        [az,el] = view; %# queries the perspective
        if ~bClicked
            az = 0;
            el = 0;
        end
        delete(findobj(gca, 'type', 'patch'));
        frame = MSkel((mSliderVal:mSliderVal),2:40);
        xm = frame(1:3:length(frame))/1000;
        ym = frame(2:3:length(frame))/1000;
        zm = frame(3:3:length(frame))/1000;

        xm = xm-xm(1);	
        ym = ym-ym(1);	
        zm = zm-zm(1);
        
        frame2 = KSkel((kSliderVal:kSliderVal),2:40);
        xk = frame2(1:3:length(frame2));
        yk = frame2(2:3:length(frame2));
        zk = frame2(3:3:length(frame2));

        xk = xk-xk(1);
        yk = yk-yk(1);
        zk = zk-zk(1);
        
        theta = pi/2;
        R = [1,0,0;0 cos(theta) -sin(theta);0 sin(theta) cos(theta)];
        rot = R*[xk;yk;zk];
        xk = rot(1,:);
        yk = rot(2,:);
        zk = rot(3,:);
        
        theta = pi;
        R = [cos(theta),-sin(theta),0; sin(theta),cos(theta),0; 0,0,1];
        rot = R*[xk;yk;zk];
        xk = rot(1,:);
        yk = rot(2,:);
        zk = rot(3,:);

        xm(isnan(xm)) = 0;
        ym(isnan(ym)) = 0;
        zm(isnan(zm)) = 0;

        [U, r, lrms] = Kabsch([xm([1,11,12,13]);ym([1,11,12,13]);zm([1,11,12,13])],[xk([1,11,12,13]);yk([1,11,12,13]);zk([1,11,12,13])]);
        rot = U*[xm;ym;zm];
        xm = rot(1,:);
        ym = rot(2,:);
        zm = rot(3,:);
      
        
        theta = pi;
        R = [cos(theta),-sin(theta),0; sin(theta),cos(theta),0; 0,0,1];
        rot = R*[xm;ym;zm];
        xm = rot(1,:);
        ym = rot(2,:);
        zm = rot(3,:);

        %xm = xm+r(1);
        %ym = ym+r(2);
        %zm = zm+r(3);
        %axis equal
        scatter3(xm,ym,zm,'filled','MarkerFaceColor',[1 0 0],'MarkerEdgeColor',[0 0 0]);
        line(xm([1,2,13,3,4]),ym([1,2,13,3,4]),zm([1,2,13,3,4]),'Color',[1 0 0]);
        line(xm([13,5,6,7]),ym([13,5,6,7]),zm([13,5,6,7]),'Color',[1 0 0]);
        line(xm([13,8,9,10]),ym([13,8,9,10]),zm([13,8,9,10]),'Color',[1 0 0]);
        line(xm([1,11]),ym([1,11]),zm([1,11]),'Color',[1 0 0]);
        line(xm([1,12]),ym([1,12]),zm([1,12]),'Color',[1 0 0]);
        hold on;
        axis equal
        scatter3(xk,yk,zk,'filled','MarkerFaceColor',[0 0 1],'MarkerEdgeColor',[0 0 0]);
        line(xk([1,2,13,3,4]),yk([1,2,13,3,4]),zk([1,2,13,3,4]));
        line(xk([13,5,6,7]),yk([13,5,6,7]),zk([13,5,6,7]));
        line(xk([13,8,9,10]),yk([13,8,9,10]),zk([13,8,9,10]));
        line(xk([1,11]),yk([1,11]),zk([1,11]));
        line(xk([1,12]),yk([1,12]),zk([1,12]));

        hold off;
        view(az,el); %# sets the perspective
        set(findobj('Tag','MSkelTime'),'String',MSkel(mSliderVal:mSliderVal,1:1))
        set(findobj('Tag','KSkelTime'),'String',KSkel(kSliderVal:kSliderVal,1:1)/1000)
        bClicked = true;
    end
end

