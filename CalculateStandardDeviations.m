function SDs = CalculateStandardDeviations( kinectPositions, qualisysPositions )
%CALCULATED Summary of this function goes here
%   Calculate Standard Deviations of each joint and return in array. Used
%   to draw ellipsoid around each joint of the SD
% kinectPositions/kinectPositions axes ordering X,Z,Y
% SDs ordering X,Y,Z
    deviations = bsxfun(@minus,kinectPositions(:,2:end),qualisysPositions(:,2:end));
    deviations(~any(isnan(deviations),2),:);
    SDs = [];
    for i = 1:(size(deviations,2)/3)
        j = (i*3)-2;
        jointDevs = deviations(:,[j,j+2,j+1]);
        jointDevs = jointDevs(~any(isnan(jointDevs),2),:);
        SDs = [SDs;std(jointDevs)];
    end
    SDs=SDs*100; % Change to CM
end

