
function [ data ] = LoadKinectData( filename )
% Load kinect skeleton data to variable
    %f = fopen(filename);
    T = readtable(strcat('./data/',filename));
    data = table2array(T(1:end,1:40));
end