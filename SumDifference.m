function res = SumDifference(a,b)
    res = 0;
    for i=1:length(a)-1
        change = a(i+1)-a(i);
        res = res + pdist2(b(i)+change,b(i+1));
    end
end