function [kDevsMag,kDevsXYZ] = CalculateRelativeDeviations( kinectPositions, qualisysPositions, kinectGT, qualisysGT )
    global qBoneGTVecs; % Qualisys bone vectors moved to origin at GT frame
    global kBoneGTVecs; % Kinect bone vectors moved to origin at GT frame
       
    if (isempty(qBoneGTVecs))
        [qBoneGTVecs,kBoneGTVecs] = GetBoneVectors(qualisysGT,kinectGT);
    end
    [boneVecsQ,boneVecsK] =  GetBoneVectors(qualisysPositions,kinectPositions);
    
    rotatedVecsK = GetRotatedVecs(boneVecsQ,boneVecsK,qBoneGTVecs);
    rotatedVecsK = reshape(rotatedVecsK',3,[])';
    kBoneGTVecs = reshape(kBoneGTVecs',3,[])';
    
    kDevsXYZ = [];
    kDevsMag = [];
    for row = 1:13:size(rotatedVecsK,1) 
        kDevsXYZ=[kDevsXYZ;arrayfun(@GetOffset,rotatedVecsK(row:(row+12),1),rotatedVecsK(row:(row+12),2),rotatedVecsK(row:(row+12),3),kBoneGTVecs(:,1),kBoneGTVecs(:,2),kBoneGTVecs(:,3),'UniformOutput',0)];
        kDevsMag=[kDevsMag;arrayfun(@GetDist,rotatedVecsK(row:(row+12),1),rotatedVecsK(row:(row+12),2),rotatedVecsK(row:(row+12),3),kBoneGTVecs(:,1),kBoneGTVecs(:,2),kBoneGTVecs(:,3))];
    end
    kDevsXYZ = cell2mat(kDevsXYZ);
    kDevsXYZ = reshape(kDevsXYZ',39,[])'; 
    kDevsXYZ = kDevsXYZ*100; % Change units to cm   
    kDevsMag = reshape(kDevsMag',13,[])'; 
    kDevsMag = kDevsMag*100; % Change units to cm   
end

function nVec = GetOffset(ax,ay,az,bx,by,bz)
   nVec = abs([ax,ay,az]-[bx,by,bz]);
end

function nVec = GetDist(ax,ay,az,bx,by,bz)
   nVec = norm([ax,ay,az]-[bx,by,bz]);
end

function rotatedVecs = GetRotatedVecs(vecQ,vecK,qBoneGTVecs)
    rotatedVecs = [];
    for row = 1:size(vecQ,1)
        rotatedVecRow=[];
        for col = 1:3:size(vecQ,2)
            rotatedVecRow=[rotatedVecRow,GetRotatedVec(vecQ(row,[col,col+1,col+2]),vecK(row,[col,col+1,col+2]),qBoneGTVecs([col,col+1,col+2]))];
        end
        rotatedVecs=[rotatedVecs;rotatedVecRow];
    end
end

function rotatedVec = GetRotatedVec(vecQ,vecK,vecQGT)
    ax = cross(vecQ,vecQGT);
    deg = rad2deg(atan2(norm(ax),dot(vecQGT,vecQ)));
    lastwarn('no') 
    rotatedVec = AxelRot(vecK',deg,ax,[])';
end

function [boneVecsQ,boneVecsK] = GetBoneVectors(vecsQ,vecsK)
    [boneVecsQ,boneVecsK]=deal([]);
    for row = 1:size(vecsQ,1)
        [boneVecsQRow,boneVecsKRow]=deal([]);
        for i = 1:(size(vecsQ,2)/3)
            j = (i*3)-2; % joint index
            o=1;
            % Get joint origin index
            if (any(j==[4,10,16,19,25,28]))
                o=j-3;
            elseif (any(j==[1,37]))
                o=4;
            elseif (any(j==[7,13,22]))
                 o=37;
            elseif (any(j==[31,34]))
                o=1;
            end
            % If origin joint or current joint is NaN then add NaN to array
            if (isnan(vecsQ(row,j)) || isnan(vecsQ(row,o)))
                boneVecsQRow=[boneVecsQRow,nan,nan,nan];
                boneVecsKRow=[boneVecsKRow,nan,nan,nan];
            else
                boneVecsQRow=[boneVecsQRow,(vecsQ(row,[j,j+1,j+2])-vecsQ(row,[o,o+1,o+2]))];
                boneVecsKRow=[boneVecsKRow,(vecsK(row,[j,j+1,j+2])-vecsQ(row,[o,o+1,o+2]))];
            end
        end
        boneVecsQ=[boneVecsQ;boneVecsQRow];
        boneVecsK=[boneVecsK;boneVecsKRow];
    end
end