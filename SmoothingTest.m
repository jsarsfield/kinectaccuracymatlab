bCalculate = 0; % Calculate or display signals
if bCalculate
    min = 1;
    max = 10;
    increment=0.1;
    ks = []; % Store kinect's best smoothed signal
    best_window = 0;
    best_alpha = 0;
    lowest_dev = inf; % Store lowest deviation of signal from ground truth
    windows = [3,5,7,9,11];
    for i = 1:numel(windows)
        fprintf('Window: %i\n',windows(i))
        for alpha=min:increment:max
            g = gausswin(windows(i),alpha);
            g = g/sum(g);
            dist = 0;
            for j = 1:size(sigs,2)
                k = sigs{j}(:,1);
                q = sigs{j}(:,2);
                temp = nanconv(k, g, 'edge');
                [d] = dtw(temp,q,1,'absolute');
                dist = dist+d;
            end
            if dist < lowest_dev
                lowest_dev = dist;
                ks = temp;
                best_window = windows(i);
                best_alpha = alpha;
                best_dist = dist;
            end
        end
    end
    fprintf('Win: %i Alpha: %f Dist: %f\n',best_window,best_alpha,best_dist);
else
    idx = 48; % Signal to plot ordered by participant, 48 is last element
    window_size = 5;
    alpha = 1.3;
    g = gausswin(window_size,alpha);
    g = g/sum(g);
    k = sigs{idx}(:,1);
    q = sigs{idx}(:,2);
    ks = nanconv(k, g, 'edge');
end

%Plot
figure;
a1 = plot(q,'Color',[0,0,1]);
hold on;
a2 = plot(k,'Color',[1,0,0]);
a3 = plot(ks,'Color',[0,0,0]);
legend([a1;a2;a3],'Qualisys','Kinect','Kinect Smoothed');



