function [ data ] = LoadQualisysData( filename )
% Load excel skeleton data to variable 
    data = tblread(strcat('./data/',filename),'\t');
    
    %tempData = reshape(data.textdata,1,[]);
    %tempData = cellfun(@str2num,cellfun(@(x) ChangeNull(x), data.textdata, 'UniformOutput', false),'UniformOutput', false);
    %data = [tempData,data.data];
    %ix=cellfun(@isempty,data.textdata);
    %data.textdata(ix)={nan};
    %A = cell2mat(data.textdata);
    %tempData(tempData==null) = 'NaN';
    %data = data(:,2:end);
    indices = [8 14 20 26 32 38 44 50];
    % calculate new joint location given two points
    for n = 1:size(indices,2)
        % Set new joint location to null if any of the x,y,z values are null
        indZero = [find(data(:,indices(n)) == 0)', find(data(:,indices(n)+3) == 0)'];
        % Calculate joint position averages
        data(:,indices(n)) = data(:,indices(n)+3) + ((data(:,indices(n)) - data(:,indices(n)+3))/2);
        data(:,indices(n)+1) = data(:,indices(n)+4) + ((data(:,indices(n)+1) - data(:,indices(n)+4))/2);
        data(:,indices(n)+2) = data(:,indices(n)+5) + ((data(:,indices(n)+2) - data(:,indices(n)+5))/2);
        
        % Set new joint location to 0 if any of the x,y,z values are 0
        data(indZero,indices(n)) = 0;
        data(indZero,indices(n)+1) = 0;
        data(indZero,indices(n)+2) = 0;
    end
    % remove superfluous columns
    indices = [11,12,13,17,18,19,23,24,25,29,30,31,35,36,37,41,42,43,47,48,49,53,54,55]; 
    fi = 1:size(data,2);
    selection=setdiff(fi,indices);
    data = data(:,selection);
end

function [v] = ChangeNull(v)
    if strcmp(v,'null')
        v = '0';
    end
end
