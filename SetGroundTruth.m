function [] = SetGroundTruth( KSkel, MSkel, filename, bGroundTruth, U, r, lrms, mGT,kGT,kGTFrame,mGTFrame,bShowDeviation)
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here
    f = figure
    h = guihandles(f);
    axis equal
    [xk,yk,zk,xmt,ymt,zmt] = deal(0);
    bClicked = false;
    startFrame = 1;
    endFrame = length(KSkel);
    startFramek = 1;
    endFramek = length(KSkel);
    rotFrameK = -1;
    armSideFrameK = -1;
    armSideFrameQ = -1;
    startFramem = 1;
    endFramem = length(MSkel);
    KSkel(:,1) = KSkel(:,1)/1000;
    MSkel(:,2:end)= MSkel(:,2:end)/1000;
    KSkelOriginal = KSkel;
    MSkelOriginal = MSkel;
    MSkel(find(MSkel==0)) = nan;
    MSkel(1) = 0;
    global stdDevs; % After creating ellipsoids stdDevs becomes magnitude of X,Y,Z; stdDev from Qualisys/Kinect offsets
    global stdDevsKinectGTXYZ;  % SD in the X Y Z coordinates for creating ellipsoids for visualisation
    global stdDevEllipsoid;     % Get the mean stdDevsKinectGTXYZ over all exercises in excel and overwrite this variable to draw SDs as ellipsoids
    global stdDevsKinectGT; % Standard deviations from Kinect's ground truth t-pose
    global meanKinectGT;    % Mean deviation from Kinect's ground truth t-pose
    global qBoneGTVecs; % Qualisys bone vectors moved to origin at GT frame
    global kBoneGTVecs; % Kinect bone vectors moved to origin at GT frame
    global kinectSignal; % Store segmented kinect signal of selected joint & axes positions
    global qualisysSignal; % Store segmented qualisys signal of selected joint & axes positions
    qBoneGTVecs=[];
    kBoneGTVecs=[];
    relDevs = []; % Stores relative deviations from Kinect GT    
    bUpdateRelDevs = 1;
    cols = [13,16,19,31;22,25,28,34];
    bNormSignals = 1;
    load('signals.mat')
    sigs_idx = size(sigs,2)+1 % signals index for writing to last element during session
    
    axes = 1;
    bViewSD = 0;
    lineWidth = 3;
    bShowRelDev = 0;    % Should we plot the relative deviation from Kinect's t-pose constant/GT otherwise show the joint positions
    % Swap columns
    %{
    for c = 1:4
        for n = 1:3
            MSkel = swap_cols(MSkel,cols(1,c)+n,cols(2,c)+n);
        end
    end
    %}
    % Joint to display deviations for
    joint = 10;
    joints = {'SpineBase','SpineMid','Neck','Head','ShoulderLeft','ElbowLeft','WristLeft','ShoulderRight','ElbowRight','WristRight','HipLeft','HipRight','SpineShoulder'};
    axesS = {'X','Y','Z'};
    if (bGroundTruth)
        SuperimposeDatasets();
    end
    
    s = uicontrol(f,'Style','slider',...
                'Min',1,'Max',length(KSkel),'Value',1,...
                'Position',[30 10 250 30],...
                'Callback',@slider_Callback,'SliderStep',[1/length(KSkel),1/length(KSkel)]);
    uicontrol('Style','text',...
        'Position',[300 20 120 20],...
        'String',0,'Tag','MSkelTime');
    uicontrol('Style','text',...
        'Position',[300 0 120 20],...
        'String',0,'Tag','KSkelTime');
    uicontrol('Style','text',...
        'Position',[400 0 120 20],...
        'String',0,'Tag','DeviationText');
    uicontrol('Style','text',...
        'Position',[400 20 120 20],...
        'String',0,'Tag','StdDevText');
    uicontrol('Style','text',...
        'Position',[510 0 120 20],...
        'String',0,'Tag','MeanText');
    uicontrol('Style','text',...
        'Position',[620 20 140 20],...
        'String',0,'Tag','KinText');
    uicontrol('Style','text',...
        'Position',[620 0 140 20],...
        'String',0,'Tag','QualText');
    uicontrol('Style', 'pushbutton', 'String', 'Save GT',...
        'Position', [0 110 80 20],...
        'Callback', @SaveGroundTruth);  
    uicontrol('Style', 'pushbutton', 'String', 'Plot deviation',...
        'Position', [0 200 80 20],...
        'Callback', @PlotDeviation); 
    uicontrol('Style', 'pushbutton', 'String', 'Calc Rel dev',...
        'Position', [0 220 80 20],...
        'Callback', @CalculateRelativeDeviation);
    uicontrol('Style', 'pushbutton', 'String', 'Start Frame',...
        'Position', [0 180 80 20],...
        'Callback', @StartFrame); 
    uicontrol('Style', 'pushbutton', 'String', 'End Frame',...
        'Position', [0 160 80 20],...
        'Callback', @EndFrame);
    uicontrol('Style', 'pushbutton', 'String', 'Rot Frame',...
        'Position', [0 140 80 20],...
        'Callback', @RotFrame);
    uicontrol('Style', 'pushbutton', 'String', 'Save',...
        'Position', [0 240 80 20],...
        'Callback', @SaveCallback);  
    jp = uicontrol('Style', 'popup',...
           'String', joints,...
           'Position', [0 340 90 50],...
           'Value',10,...
           'Callback', @SetJoint); 
    uicontrol('Style', 'popup',...
       'String', axesS,...
       'Position', [0 320 90 50],...
       'Value',2,...
       'Callback', @SetAxes); 
   uicontrol('Style', 'checkbox',...
        'Position', [0 90 20 20],...
        'Value',0,...
        'Callback', @SetViewSD); 
    uicontrol('Style', 'checkbox',...
        'Position', [0 70 20 20],...
        'Value',0,...
        'Callback', @SetRelDev);
    uicontrol('Style', 'checkbox',...
        'Position', [0 50 20 20],...
        'Value',1,...
        'Callback', @SetNormSignals);
    
    function SetRelDev(source,callbackdata)
        bShowRelDev = get(source,'Value');
    end

    function SetNormSignals(source,callbackdata)
        bNormSignals = get(source,'Value');
    end
    
    function SaveCallback(source,callbackdata)
        % Save stdDevs
        % SaveData('stdDevs.txt',filename,stdDevs);
        % Save stdDevsKinectGT
        disp('saving stdDevsKinectGT')
        disp(stdDevsKinectGT);
        SaveData('stdDevsKinectGT.txt',filename,stdDevsKinectGT);
        % Save meanKinectGT
        disp('saving meanKinectGT')
        disp(meanKinectGT);
        SaveData('meanKinectGT.txt',filename,meanKinectGT);
        % Save stdDevsKinectGTXYZ
        disp('saving stdDevsKinectGTXYZ')
        disp(stdDevsKinectGTXYZ);
        SaveData('stdDevsKinectGTXYZ.txt',filename,stdDevsKinectGTXYZ);
    end

    function SetViewSD(source,callbackdata)
        bViewSD = get(source,'Value');
        sliderCallback = get(s,'Callback');
        sliderCallback(s,[],h);
    end
    function A = swap_cols(A,i,j)
        A(:,[i j]) = A(:,[j i]);
    end
    function SetJoint(source,callbackdata)
        joint = get(source,'Value');
    end
    function SetAxes(source,callbackdata)
        val = get(source,'Value');
        if (val==1)
            axes = -1;
        elseif (val==2)
            axes = 1;
        else
            axes = 0;
        end
    end
    function StartFrame(hObject, EventData)
        startFramek = round(get(s,'value'));
        %{
        startFramek = round(get(s,'value'));
        [~, idx] = min(abs((MSkel(:,1)-MSkel(1,1))-(KSkel(startFramek:startFramek,1:1)-KSkel(1,1))));
        startFramem = idx;
        %}
    end
    function EndFrame(hObject, EventData)
        endFramek = round(get(s,'value'));
        %{
        endFramek = round(get(s,'value'));
        [~, idx] = min(abs((MSkel(:,1)-MSkel(1,1))-(KSkel(startFramek:startFramek,1:1)-KSkel(1,1))));
        endFramem = idx;
        %}
    end
    function RotFrame(hObject, EventData)
        %rotFrameK = round(get(s,'value'));
        armSideFrameK = round(get(s,'value'));
        [~, armSideFrameQ] = min(abs((MSkel(:,1)-MSkel(1,1))-(KSkel(armSideFrameK:armSideFrameK,1:1)-KSkel(1,1))));
    end
    function PlotDeviation(hObject, EventData)
        % Get indices of MSkel that minimise time difference KSkel startFramek:endFramek
        MSkelIdx = arrayfun(@MinVal, KSkel(startFramek:endFramek,1:1));
        % Get MSkel offset vectors from mGT
        mOffset = bsxfun(@minus,MSkel(MSkelIdx,2:40),mGT);
        % Determine kSkel ground truths for frames startFramek:endFramek
        kGTs = bsxfun(@plus,mOffset,kGT);
        % Determine difference between the grounds truths and KSkel points
        kDeviations = KSkel(startFramek:endFramek,2:40)-kGTs;
        kDeviations = reshape(kDeviations',3,[])';
        % Turn deviations into vector magnitudes
        kMag = arrayfun(@GetMags,kDeviations(:,1),kDeviations(:,2),kDeviations(:,3)); 
        kMag = reshape(kMag',13,[])';
        plot(KSkel(startFramek:endFramek,1:1),kMag(:,1),'Color',[1,0,0]);
        %hold on;
        %plot([10,11,12,13,14,14.1],[0.8,0.2,0.1,0.5,0.6,0.8],'Color',[0,0,1]);
        legend('spine_base');
        axis([-inf,inf,0,inf]);
    end
    function idx = MinVal(kTime)
        [~, idx] = min(abs((MSkel(:,1)-MSkel(1,1))-(kTime-KSkel(1,1))));
    end
    function SaveGroundTruth(hObject, EventData)
        if (armSideFrameK ~= -1)
            kGTFrame = round(get(s,'value'));
            [~, mGTFrame] = min(abs((MSkel(:,1)-MSkel(1,1))-(KSkel(kGTFrame:kGTFrame,1:1)-KSkel(1,1))));
            frame = MSkel((mGTFrame:mGTFrame),2:end);
            frame2 = KSkel((kGTFrame:kGTFrame),2:end);
            mGT = frame;
            kGT = frame2;

            % Rotate around X to find rotation that minimises deviation of
            % wristleft joint at arms to side and arms tpose
            U = FindRotXDevMin(KSkel(armSideFrameK:armSideFrameK,2:end),MSkel(armSideFrameQ:armSideFrameQ,2:end),KSkel(kGTFrame:kGTFrame,2:end),MSkel(mGTFrame:mGTFrame,2:end),U);

            save(strcat('data/',strrep(filename, '.', 'GT.mat')),'kGTFrame','mGTFrame','U', 'r', 'lrms', 'mGT', 'kGT');
            UpdateGroundTruth();
        end
    end
    
    function UpdateGroundTruth()
        bGroundTruth = 1;
        SuperimposeDatasets()
    end
    
    % Overlap via rotation & translation Qualisys & Kinect datasets
    function SuperimposeDatasets()
        KTime = KSkel(:,1);
        MTime = MSkel(:,1);
        KSkel = KSkel(:,2:end);
        MSkel = MSkel(:,2:end);
        
        zeroIdx = find(reshape(MSkel,1,[]) == 0);
        
        MSkel(:,1:3:end) = MSkel(:,1:3:end)-mGT(1);	
        MSkel(:,2:3:end) = MSkel(:,2:3:end)-mGT(2);	
        MSkel(:,3:3:end) = MSkel(:,3:3:end)-mGT(3);	
        
        KSkel(:,1:3:end) = KSkel(:,1:3:end)-kGT(1);	
        KSkel(:,2:3:end) = KSkel(:,2:3:end)-kGT(2);	
        KSkel(:,3:3:end) = KSkel(:,3:3:end)-kGT(3);	
          
        theta = pi/2;  
        R = [1,0,0;0 cos(theta) -sin(theta);0 sin(theta) cos(theta)];
        KSkel = reshape(KSkel',3,[])';
        KSkel = arrayfun(@RotateDataset,KSkel(:,1),KSkel(:,2),KSkel(:,3),'UniformOutput',0); 
        kGT = cell2mat(arrayfun(@RotateDataset,kGT(1:3:end),kGT(2:3:end),kGT(3:3:end),'UniformOutput',0)); 
        KSkel = cell2mat(KSkel);
        theta = pi;
        
        R = [cos(theta),-sin(theta),0; sin(theta),cos(theta),0; 0,0,1];
        KSkel = arrayfun(@RotateDataset,KSkel(:,1),KSkel(:,2),KSkel(:,3),'UniformOutput',0); 
        kGT = cell2mat(arrayfun(@RotateDataset,kGT(1:3:end),kGT(2:3:end),kGT(3:3:end),'UniformOutput',0)); 
        KSkel = cell2mat(KSkel);
        
        % Scale left shoulder joint. scaleFactor is determined by
        % shifting qualisys & kinect y shoulder joint to zero mean then
        % dividing (mrdivide linear transformation) the resultant vectors from each other to produce a scaling factor.
       %{
        scaleFactor = 1; %2.489717
        os = (KSkel(5,3)*scaleFactor)-KSkel(5,3);
        KSkel(5:13:end,3) = bsxfun(@minus,(KSkel(5:13:end,3)*scaleFactor),os);
        %}
        KSkel = reshape(KSkel',39,[])';
    
        R = U;
        MSkel = reshape(MSkel',3,[])';
        MSkel = arrayfun(@RotateDataset,MSkel(:,1),MSkel(:,2),MSkel(:,3),'UniformOutput',0);  
        mGT = cell2mat(arrayfun(@RotateDataset,mGT(1:3:end),mGT(2:3:end),mGT(3:3:end),'UniformOutput',0)); 
        MSkel = cell2mat(MSkel);
        
        MSkel(:,1) = MSkel(:,1)+r(1);
        MSkel(:,2) = MSkel(:,2)+r(2);
        MSkel(:,3) = MSkel(:,3)+r(3);
                
        MSkel = reshape(MSkel',39,[])';
        
        MU = reshape(MSkel,1,[]);
        MU(zeroIdx) = 0;
        MSkel = reshape(MU',[],39);

        KSkel = [KTime,KSkel];
        MSkel = [MTime,MSkel];
            
        function point = RotateDataset(x,y,z)
            point = (R*[x;y;z])';
        end
    end
            
    function slider_Callback(hObject, eventdata, handles) 
        skelPlot = subplot(2,1,1);
        grid on
        ax = gca
        if (strcmp(ax.XMinorGrid,'off'))
            grid minor
        end
        colormap(skelPlot,gray)
        slider_value = get(hObject,'Value');
        slider_value = round(slider_value);
        set(hObject, 'Value', slider_value);
        fprintf('%i\n',slider_value);
        [az,el] = view; %# queries the perspective
        if ~bClicked
            az = 0;
            el = 0;
        end
        %  delete children in the plot, lines etc
        children = get(gca, 'children');
        if(~isempty(children))
            delete(children);
        end
        [~, idx] = min(abs((MSkel(:,1)-MSkel(1,1))-(KSkel(slider_value:slider_value,1:1)-KSkel(1,1))));
       
        frame = MSkel((idx:idx),2:end);
        xm = frame(1:3:length(frame));
        ym = frame(2:3:length(frame));
        zm = frame(3:3:length(frame));
        
        frame2 = KSkel((slider_value:slider_value),2:end);
        xk = frame2(1:3:length(frame2));
        yk = frame2(2:3:length(frame2));
        zk = frame2(3:3:length(frame2));
        
        
        if (~bGroundTruth) 
            rotJoints = [11,12,2];
            up = 1;
            down = 0.05;
            rotWeights = [up,up,up,up,up];
            % Get values from rotation frame
            [xmr,ymr,zmr,xkr,ykr,zkr]=deal([]);
            if (rotFrameK ~= -1)
                rotWeights=[rotWeights,rotWeights];
                [~, rotFrameQ] = min(abs((MSkel(:,1)-MSkel(1,1))-(KSkel(rotFrameK:rotFrameK,1:1)-KSkel(1,1))));
                frameRQ = MSkel((rotFrameQ:rotFrameQ),2:end);
                xmr = frameRQ(1:3:length(frameRQ));
                ymr = frameRQ(2:3:length(frameRQ));
                zmr = frameRQ(3:3:length(frameRQ));
                frameRK = KSkel((rotFrameK:rotFrameK),2:end);
                xkr = frameRK(1:3:length(frameRK));
                ykr = frameRK(2:3:length(frameRK));
                zkr = frameRK(3:3:length(frameRK)); 
            end
            
            xm = xm-xm(1);	
            ym = ym-ym(1);	
            zm = zm-zm(1);
            if (rotFrameK ~= -1)
                xmr = xmr-xmr(1);	
                ymr = ymr-ymr(1);	
                zmr = zmr-zmr(1);
            end
            
            xk = xk-xk(1);
            yk = yk-yk(1);
            zk = zk-zk(1);
            if (rotFrameK ~= -1)
                xkr = xkr-xkr(1);
                ykr = ykr-ykr(1);
                zkr = zkr-zkr(1);
            end
            
            theta = pi/2;  
            R = [1,0,0;0 cos(theta) -sin(theta);0 sin(theta) cos(theta)];
            rot = R*[xk;yk;zk];
            xk = rot(1,:);
            yk = rot(2,:);
            zk = rot(3,:);
            if (rotFrameK ~= -1)
                rot = R*[xkr;ykr;zkr];
                xkr = rot(1,:);
                ykr = rot(2,:);
                zkr = rot(3,:);
            end

            theta = pi;
            R = [cos(theta),-sin(theta),0; sin(theta),cos(theta),0; 0,0,1];
            rot = R*[xk;yk;zk];
            xk = rot(1,:);
            yk = rot(2,:);
            zk = rot(3,:);
            if (rotFrameK ~= -1)
                rot = R*[xkr;ykr;zkr];
                xkr = rot(1,:);
                ykr = rot(2,:);
                zkr = rot(3,:);
            end
            % HipLeft,HipRight,SpineShoulder used to align the datasets using Kabsch algorithm
            if (rotFrameK ~= -1)
                if (~any(isnan([xm(rotJoints),xmr(rotJoints)])) && ~any(isnan([ym(rotJoints),ymr(rotJoints)])) && ~any(isnan([zm(rotJoints),zmr(rotJoints)])))
                    [U, r, lrms] = Kabsch([[xm(rotJoints),xmr(rotJoints)];[ym(rotJoints),ymr(rotJoints)];[zm(rotJoints),zmr(rotJoints)]],[[xk(rotJoints),xkr(rotJoints)];[yk(rotJoints),ykr(rotJoints)];[zk(rotJoints),zkr(rotJoints)]]); 
                end 
            else
                if (~any(isnan(xm(rotJoints))) && ~any(isnan(ym(rotJoints))) && ~any(isnan(zm(rotJoints))))
                    [U, r, lrms] = Kabsch([xm(rotJoints);ym(rotJoints);zm(rotJoints)],[xk(rotJoints);yk(rotJoints);zk(rotJoints)]); 
                end 
            end
            rot = U*[xm;ym;zm];
            xm = rot(1,:);
            ym = rot(2,:);
            zm = rot(3,:);  
                 
            % Redo Kabsch on different joints for a suitable translation
            if (~any(isnan(xm(13))) && ~any(isnan(ym(13))) && ~any(isnan(zm(13))))
                [a, r, b] = Kabsch([xm(13);ym(13);zm(13)],[xk(13);yk(13);zk(13)]);
            end
            xm = xm+r(1);
            ym = ym+r(2);
            zm = zm+r(3);    
        end
        % Get indices of MSkel that minimise time difference KSkel startFramek:endFramek
        MSkelIdx = arrayfun(@MinVal, KSkel(startFramek:endFramek,1:1));
        KSkelMin = KSkel(startFramek:endFramek, :);
        % Calculate SDs for each joint
        if (bViewSD)
            %stdDevs = CalculateStandardDeviations(KSkelMin,MSkel(MSkelIdx(:),:));
        end
        ind = find(frame(1:3:length(frame)) ~=0);
        % Scale from Metres to CM
        if (~any(isnan(xm(13))) && ~any(isnan(ym(13))) && ~any(isnan(zm(13))))
            [a, r, b] = Kabsch([xm(13);ym(13);zm(13)],[xk(13);yk(13);zk(13)]);
            xm = (xm+r(1))*100;
            ym = (ym+r(2))*100;
            zm = (zm+r(3))*100;
        else
            xm = xm*100;
            ym = ym*100;
            zm = zm*100;
        end
        xk = xk*100;
        yk = yk*100;
        zk = zk*100;
        %scatter3(xm,ym,zm,'filled','MarkerFaceColor',[1 0 0],'MarkerEdgeColor',[0 0 0]);
        s1 = line(xm(intersect([1,2,13,3,4],ind,'stable')),ym(intersect([1,2,13,3,4],ind,'stable')),zm(intersect([1,2,13,3,4],ind,'stable')),'Color',[1 0 0],'LineWidth',lineWidth);
        line(xm(intersect([13,5,6,7],ind,'stable')),ym(intersect([13,5,6,7],ind,'stable')),zm(intersect([13,5,6,7],ind,'stable')),'Color',[1 0 0],'LineWidth',lineWidth);
        line(xm(intersect([13,8,9,10],ind,'stable')),ym(intersect([13,8,9,10],ind,'stable')),zm(intersect([13,8,9,10],ind,'stable')),'Color',[1 0 0],'LineWidth',lineWidth);
        line(xm(intersect([1,11],ind,'stable')),ym(intersect([1,11],ind,'stable')),zm(intersect([1,11],ind,'stable')),'Color',[1 0 0],'LineWidth',lineWidth);
        line(xm(intersect([1,12],ind,'stable')),ym(intersect([1,12],ind,'stable')),zm(intersect([1,12],ind,'stable')),'Color',[1 0 0],'LineWidth',lineWidth);
        hold on;
        if (~bViewSD || isempty(stdDevEllipsoid))
            scatter3(xk,yk,zk,'filled','MarkerFaceColor',[0 0 1],'MarkerEdgeColor',[0 0 0]);
        end
        % Model SDs as ellipsoids
        if (bViewSD && ~isempty(stdDevEllipsoid))
            stdDevEllipsoid = reshape(stdDevEllipsoid,3,[])';
            for i = 1:size(stdDevEllipsoid,1)
                % Plot SD ellipsoid, divide by 2 as parameter is radius NOT
                % diameter and size parameters are X, Z, Y
                [x, y, z] = ellipsoid(xk(i),yk(i),zk(i),stdDevEllipsoid(i,1)/2,stdDevEllipsoid(i,3)/2,stdDevEllipsoid(i,2)/2,100); 
                surf(x, y, z,'EdgeColor','none','FaceColor',[0 0 0],'SpecularExponent',4); 
            end
            stdDevEllipsoid = reshape(stdDevEllipsoid',1,[]);
            %stdDevs = sqrt(sum(stdDevs.^2,2));
            %stdDevs = stdDevs';
        end
        light('Color',[1 1 1],'Style','infinite','Position',[-0.5 0.5 0.5])
        s2 = line(xk([1,2,13,3,4]),yk([1,2,13,3,4]),zk([1,2,13,3,4]),'LineWidth',lineWidth);
        line(xk([13,5,6,7]),yk([13,5,6,7]),zk([13,5,6,7]),'LineWidth',lineWidth);
        line(xk([13,8,9,10]),yk([13,8,9,10]),zk([13,8,9,10]),'LineWidth',lineWidth);
        line(xk([1,11]),yk([1,11]),zk([1,11]),'LineWidth',lineWidth);
        line(xk([1,12]),yk([1,12]),zk([1,12]),'LineWidth',lineWidth);
        
        % Undo scale from CM to metres
        xm = xm./100;
        ym = ym./100;
        zm = zm./100;
        xk = xk./100;
        yk = yk./100;
        zk = zk./100;
        hold off;
        view(az,el); %# sets the perspective
        axis equal;
        xlabel('X-axis(cm)');
        ylabel('Z-axis(cm)');
        zlabel('Y-axis(cm)');
        legend([s1,s2],{'Qualisys','Kinect'});
        set(findobj('Tag','MSkelTime'),'String',sprintf('%s%f','Q time: ',MSkel(idx:idx,1:1)));
        set(findobj('Tag','KSkelTime'),'String',sprintf('%s%f','K time: ',KSkel(slider_value:slider_value,1:1)));
        
        % Plot the Qualisys/Kinect signals of a joint along a specific axis
        if (~bShowRelDev)
            plot2 = subplot(2,1,2);
            % Remove NaNs from signal i.e. get indicies of non NAN values
            ids = find(~isnan(MSkel(MSkelIdx,(joint*3)+axes)));
            % Normalise signals to zero mean
            if (bNormSignals)
                kinectSignal = (KSkelMin(:,(joint*3)+axes)) - mean(KSkelMin(:,(joint*3)+axes));
                qualisysSignal = (MSkel(MSkelIdx(:),(joint*3)+axes)) - mean(MSkel(MSkelIdx(ids),(joint*3)+axes));
            else
                kinectSignal = (KSkelMin(:,(joint*3)+axes));
                qualisysSignal = (MSkel(MSkelIdx(:),(joint*3)+axes));
            end
            kinectSignal=kinectSignal*100;
            qualisysSignal=qualisysSignal*100;
            k = kinectSignal;
            q = qualisysSignal;
            ids = find(~isnan(q)); % remove nans
            q = q(ids);
            k = k(ids);
            sigs{sigs_idx} = [k,q]
            save('signals.mat','sigs')
            % plot signals
            p1 = plot(KSkelMin(:,1),kinectSignal,'Color',[0 0 1]);
            hold on;
            p2 = plot(MSkel(MSkelIdx,1),qualisysSignal,'Color',[1 0 0]);
            set(plot2,'ButtonDownFcn',@PlotClickCallback);
            line([KSkel(slider_value:slider_value,1:1) KSkel(slider_value:slider_value,1:1)], get(gca, 'ylim'));
            line([KSkel(startFramek:startFramek,1:1) KSkel(startFramek:startFramek,1:1)], get(gca, 'ylim'));
            line([KSkel(endFramek:endFramek,1:1) KSkel(endFramek:endFramek,1:1)], get(gca, 'ylim'));
            hold off;
            xlabel('time(sec)');
            ylabel('position(cm)');
            legend([p1,p2],{'Kinect','Qualisys'});
            %set(findobj('Tag','KinText'),'String',sprintf('%s%.2f','Kinect Pos(cm): ',KSkelMin(slider_value,(joint*3)+axes)*100));
            %set(findobj('Tag','QualText'),'String',sprintf('%s%.2f','Qualisys Pos(cm): ',MSkel(idx,(joint*3)+axes)*100));
        end
        if (bGroundTruth)
            % If we have a GT, plot relative deviations from Kinect's
            % t-pose constant/ground truth
            if (bShowRelDev)             
                plot2 = subplot(2,1,2);
                if (bUpdateRelDevs)
                    [relDevs,relDevsXYZ] = CalculateRelativeDeviations(KSkelMin(:,2:end),MSkel(MSkelIdx(:),2:end),KSkel((kGTFrame:kGTFrame),2:end),MSkel((mGTFrame:mGTFrame),2:end)); 
                    stdDevsKinectGTXYZ = nanstd(relDevsXYZ);
                    stdDevsKinectGT = nanstd(relDevs);
                    meanKinectGT = nanmean(relDevs);
                    bUpdateRelDevs = 0;
                end
                relDevsCurrentJoint = relDevs(:,joint);
                plot(KSkelMin(:,1:1),relDevsCurrentJoint(:),'Color',[1,0,0]);
                set(plot2,'ButtonDownFcn',@PlotClickCallback);
                line([KSkel(slider_value:slider_value,1:1) KSkel(slider_value:slider_value,1:1)], get(gca, 'ylim'));
                line([KSkel(startFramek:startFramek,1:1) KSkel(startFramek:startFramek,1:1)], get(gca, 'ylim'));
                line([KSkel(endFramek:endFramek,1:1) KSkel(endFramek:endFramek,1:1)], get(gca, 'ylim'));
                xlabel('time(sec)');
                ylabel('deviation(cm)');
                axis([-inf,inf,0,inf]);
            end
            %{
            subplot(3,1,2);
            %if (bShowDeviation)  
            % Get MSkel offset vectors from mGT
            mOffset = bsxfun(@minus,MSkel(MSkelIdx,2:end),MSkel((mGTFrame:mGTFrame),2:end));
            % Get KSkel offset vectors from kGT
            kOffset = bsxfun(@minus,KSkel(:,2:end),KSkel((kGTFrame:kGTFrame),2:end));
            % Calculate differences in the offsets
            kDeviations = bsxfun(@minus,kOffset,mOffset);
            kDeviations = reshape(kDeviations',3,[])';
            % Turn deviations into vector magnitudes
            kMag = arrayfun(@GetMags,kDeviations(:,1),kDeviations(:,2),kDeviations(:,3)); 
            kMag = reshape(kMag',13,[])';
            kMag = kMag(:,joint)*100;
            kMagSE = kMag(startFrame:endFrame);
            stdDev = std(kMagSE(find(~isnan(kMagSE))));
            meanV = mean(kMagSE(find(~isnan(kMagSE))));
            set(findobj('Tag','DeviationText'),'String',sprintf('%s%.2f','Deviation(cm): ',kMag(slider_value:slider_value)));
            set(findobj('Tag','StdDevText'),'String',sprintf('%s%.2f','Std Dev(cm): ',stdDev)); 
            set(findobj('Tag','MeanText'),'String',sprintf('%s%.2f','Mean(cm): ',meanV));
            plot(KSkel(startFramek:endFramek,1:1),kMag(:),'Color',[1,0,0]);
            %hold on;
            %scatter(KSkel(slider_value:slider_value,1:1),0);
            hold on;
            line([KSkel(slider_value:slider_value,1:1) KSkel(slider_value:slider_value,1:1)], get(gca, 'ylim'));
            hold on;
            line([KSkel(startFrame:startFrame,1:1) KSkel(startFrame:startFrame,1:1)], get(gca, 'ylim'),'Color',[0,1,0]);
            hold on;
            line([KSkel(endFrame:endFrame,1:1) KSkel(endFrame:endFrame,1:1)], get(gca, 'ylim'),'Color',[0,1,0]);
            hold on;
            line([KSkel(startFrame:startFrame,1:1),KSkel(endFrame:endFrame,1:1)], [meanV,meanV],'Color',[0,0,0]);
            hold on;
            line([KSkel(startFrame:startFrame,1:1),KSkel(endFrame:endFrame,1:1)], [meanV+stdDev,meanV+stdDev],'Color',[0.5,0.5,0.5]);
            hold on;
            line([KSkel(startFrame:startFrame,1:1),KSkel(endFrame:endFrame,1:1)], [meanV-stdDev,meanV-stdDev],'Color',[0.5,0.5,0.5]);
            hold off;
            legend({'Deviation','Current frame','Action Start','Action End','mean','std dev'});
            xlabel('time(sec)');
            ylabel('deviation(cm)');
            axis([-inf,inf,0,inf]);
            %}
            %*******************Position plotting******************************************************* 
            % s1 = Kinect signal; 
            % s2 = Qualisys signal; 
            %s3 = (smooth(KSkel(ids,(joint*3)+axes),'moving'));%  - mean(smooth(KSkel(ids,(joint*3)+axes),'moving')));
            % Multiply signals to cm from metres 
            %{
            s3=s3*100;
            % transpose signals and scale s1 & s3 signals 
            s4 = s1';
            scaleFactor =(s2'/s1');
            s1 = s1'*scaleFactor;
            s3 = s3'*(s2'/s3');
            %}
            
             %{
            p2 = plot(KSkel(ids,1),s3,'Color',[0 0 1]);         
            hold on;
            p3 = plot(KSkel(ids,1),s1,'Color',[0 0 0]);
            hold on;
            p4 = plot(KSkel(ids,1),s4,'Color',[0 1 1]);
            s2 = s2';
            %}
            %sprintf('pdist2 smooth: %f',pdist2(s2,s3))
            % TODO Find the average of all scale factors for left shoulder
            % vertical axis in all exercises and determine standard
            % deviation. Low SD is good for common scale factor to apply.
            %sprintf('Scale Fac: %f',scaleFactor)
            % Print similarity of Qualisis and Kinect signals. Value in cm
            % lower is better
            %sprintf('Kinect Scaled Smoothed: %f',SumDifference(s2,s3))
            %sprintf('Kinect Scaled : %f',SumDifference(s2,s1))
            %sprintf('Kinect : %f',SumDifference(s2,s4))
            
        end
        bClicked = true;
    end

    function PlotClickCallback ( objectHandle , eventData )
        [~, idx] = min(abs(KSkel(:,1)-objectHandle.CurrentPoint(1)));
        if (eventData.Button == 1)
            set(s, 'Value', idx);
            sliderCallback = get(s,'Callback');
            sliderCallback(s,[],h);
        elseif (eventData.Button == 2)
            startFramek = idx;
            bUpdateRelDevs = 1;
        else
            endFramek = idx;
            bUpdateRelDevs = 1;
        end
    end

    function CalculateRelativeDeviation(hObject, EventData)
        % Get KSkel start and end frames
        KSkelMin = KSkel(startFramek:endFramek, :);
        % Get indices of MSkel that minimise time difference KSkel startFramek:endFramek
        QSkelIdx = arrayfun(@MinVal, KSkelMin(:,1:1));
        KSkelMin = KSkelMin*100;
        QSkelMin = MSkel(QSkelIdx, :)*100;
        % Remove NaNs from signal i.e. get indicies of non NAN values
        ids = find(~isnan(QSkelMin(:,(joint*3)+axes)));
        kRelChange = arrayfun(@RelChange,KSkelMin(ids(1:end-1),(joint*3)+axes),KSkelMin(ids(2:end),(joint*3)+axes));
        qRelChange = arrayfun(@RelChange,QSkelMin(ids(1:end-1),(joint*3)+axes),QSkelMin(ids(2:end),(joint*3)+axes));
        difference = arrayfun(@GetDifference,kRelChange,qRelChange);
        meanDifference = mean(difference);
        set(findobj('Tag','KinText'),'String',sprintf('%s%.2f','Rel Dev(cm): ',meanDifference));
    end

    function res = RelChange(prevVal,CurVal)
        res = CurVal - prevVal;
    end

    function res = GetDifference(k,q)
        res = abs(k-q);
    end
end


