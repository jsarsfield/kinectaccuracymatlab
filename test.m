f = figure;
global index;
global q;
global qgt;
global k;
global kgt;
global qRotated
index=1;
qWrist = [2,1,0];
qElbow = [0,0,0];
%if (qWrist(1) > 0)
    kWrist = [2,0.5,0];
%else
%    kWrist = [qWrist(1)+1,qWrist(2),qWrist(3)];
%end
% Move vectors to origin
qgt = [1,1,0]-[0,0,0];
kgt = [1.7393,1.1068,0]-[0,0,0];
q = qWrist-qElbow;
k = kWrist-qElbow;
testF(qgt,[0,1,0]);
testF(q,[1,0,0]);
testF(kgt,[1,0,1]);
testF(k,[0,0,1]);
%Rotation matrix to rotate new Kinect position vector to align with
%qualisys GT bone axis

%R = vrrotvec2mat(vrrotvec(q,qgt));
%kRotated = (R*k')';
ax = cross(q,qgt);
deg = rad2deg(atan2(norm(ax),dot(qgt,q)));
kRotated = AxelRot(k',deg,ax,[]);
tic
%deg = atan2(norm(ax),dot(qgt,q));
%kRotated = rodrigues_rot(k',ax',deg); %AxelRot(k',deg,ax,[]);
toc
%AxelRot(k',deg,ax,[]);
pdist2(kRotated',kgt,'euclidean')
%kRotated = quatrotate(dcm2quat(R),k);
%qRotated = quatrotate(dcm2quat(R),q);
%v=vrrotvec(q,qgt);
%[kRotated, R, t] = AxelRot(k', radtodeg(v(4)), v(1:3),[])
%kRotated = rotVecAroundArbAxis(qgt/norm(qgt),k/norm(k),pi);
%kRotated=rodrigues_rot(k,v(1:3),v(4));
%testF(qRotated/norm(qRotated),[0,0,0]);
testF(AxelRot(q',deg,ax,[]),[0.5,0.5,0.5]);
testF(kRotated,[0,0,0]);
axis equal;
set(f,'KeyPressFcn',@kp);





 