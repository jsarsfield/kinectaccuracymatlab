clear;
filename = 'a42p5.';
bTrimAction = 0;
bShowDeviation = 1;

if (exist(strcat('data/',strcat(filename,'mat')), 'file'))
    bTrimAction = 0;
else
    bTrimAction = 1;
end


if (bTrimAction)
    KSkel = LoadKinectData(strcat(filename,'txt'));
    MSkel = LoadQualisysData(strcat(filename, 'tsv'));
    TrimActions(KSkel, MSkel,filename);
else
    load(strcat('data/',strcat(filename, 'mat')));
    if exist(strcat('data/',strrep(filename, '.', 'GT.mat')), 'file') == 2
        load(strcat('data/',strrep(filename, '.', 'GT.mat')));
        SetGroundTruth(KSkel, MSkel, filename, 1,U,r,lrms,mGT,kGT,kGTFrame,mGTFrame,bShowDeviation); 
    else
        [U, r, lrms] = deal(0);
        SetGroundTruth(KSkel, MSkel, filename, 0,U,r,lrms,0,0,1,1);
    end
end




