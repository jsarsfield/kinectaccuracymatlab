function SaveData( fileToSaveTo, filename, data )
%   Save data to file, overwrite line if data already exists for this
%   participant otherwise create new line
    fid = fopen(fileToSaveTo, 'r+');
    lines = textscan(fid,'%s','delimiter','\n');
    fclose(fid);
    lines = lines{1};
    if (strcmp(lines(1),'result'))
        lines = strrep(lines(2:end),'"','');
    end
    result = [];
    for i=1:length(lines)
        line = lines(i);
        if (strcmp(line{1},filename(1:2)))
            str = strrep(strrep(strcat(filename,mat2str(data)),'[',' '),']','');
            bExists = 0;
            %Check if filename already exists in file
            for j=i:length(lines)
                line = lines(j);
                if (~isempty(findstr(strcat(strtok(line{1},'.'),'.'),filename)))
                   bExists = j;
                   break;
                end
            end
            if (bExists == 0)
                if (i ~= length(lines))
                    result = [lines(1:i)',str,lines(i+1:end)']';
                else
                     result = [lines(1:i)',str]';
                end  
            else
                if (j ~= length(lines))
                    result = [lines(1:j-1)',str,lines(j+1:end)']';
                else
                     result = [lines(1:j-1)',str]';
                end  
            end
            break;
        end
    end
    resultT = cell2table(result);
    writetable(resultT,fileToSaveTo);
end
%{
EMPTY FILE TEMPLATE
result
'SpineBase','SpineMid','Neck','Head','ShoulderLeft','ElbowLeft','WristLeft','ShoulderRight','ElbowRight','WristRight','HipLeft','HipRight','SpineShoulder'
a1
a2
a3
a4
a5
%}
